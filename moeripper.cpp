/*
    moeripper
    Copyright (C) 2014-2024  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <iostream>
#include <string>
#include <vector>


#include <dvdread/dvd_reader.h>
#include <dvdread/ifo_read.h>
#include <fmt/core.h>


/*
 * Relevant literature:
 *
 * - https://en.wikibooks.org/wiki/Inside_DVD-Video
 * - http://www.mpucoder.com/DVD/
 */



int main(int argc, char** argv)
{
    std::vector<std::string> args(argv + 1, argv + argc);

    if (args.empty()) {
        std::cerr << "Usage: moeripper_cpp COMMAND\n"
            "Available commands:\n"
            "\trip DVD_TITLE [OUTPUT_FILE_PATH] [DVD_DEVICE]\n"
            "\t\tRips a specific DVD title.\n"
            "\tlist [DVD_DEVICE]\n"
            "\t\tList all titles on the DVD.\n";
        return EXIT_FAILURE;
    }

    if (args[0] == "rip") {
        //Temporary functionality: Read one DVD title via dvdread
        //and store it somewhere.
        //This is meant to be used in conjunction with the moeripper.rb
        //ruby program until the latter is fully replaced.


        if (args.size() < 2) {
            //DVD title parameter is missing.
            return EXIT_FAILURE;
        }

        int title_number = 0;
        try {
            title_number = std::stoi(args[1]);
        } catch (std::invalid_argument& e) {
            std::cerr << fmt::format(
                "The title number \"{0}\" is invalid.",
                args[1]
                ) << std::endl;
            return EXIT_FAILURE;
        }
        if (title_number == 0) {
            std::cerr << "The title number 0 is an invalid number. Titles start with the number 1." << std::endl;
            return EXIT_FAILURE;
        }

        std::string dvd_device = "/dev/sr0";
        std::string output_file_path = "dvd.mpg";

        if (args.size() >= 3) {
            //Read the output file path:
            output_file_path = args[2];
        }

        if (args.size() >= 4) {
            //Read the dvd device:
            dvd_device = args[3];
        }

        //Check the output:

        std::cerr << fmt::format("Reading title {0} from device {1}.", title_number, dvd_device)
                  << std::endl;

        auto handle = DVDOpen(dvd_device.c_str());

        if (handle == 0) {
            std::cerr << "Cannot open the DVD device." << std::endl;
            return EXIT_FAILURE;
        }

        auto ifo_handle = ifoOpen(handle, 0);
        if (ifo_handle == nullptr) {
            std::cerr << "Cannot read title information." << std::endl;
            return EXIT_FAILURE;
        }

        //Read data from the IFO video manager file:

        if (ifo_handle->tt_srpt == nullptr) {
            std::cerr << "Cannot read title information." << std::endl;
            return EXIT_FAILURE;
        }
        if (ifo_handle->tt_srpt->nr_of_srpts < 1
            || ifo_handle->tt_srpt->title == nullptr) {
            std::cerr << "This DVD has no titles." << std::endl;
            return EXIT_FAILURE;
        }

        //Check if the specified title number is valid:
        if (title_number > ifo_handle->tt_srpt->nr_of_srpts) {
            std::cerr << fmt::format(
                "The title {0} is out of range. There are only {1} title(s) on the medium.",
                title_number,
                (uint16_t)ifo_handle->tt_srpt->nr_of_srpts
                ) << std::endl;
            return EXIT_FAILURE;
        }

        //Get the title information:
        auto title_info = ifo_handle->tt_srpt->title[title_number - 1];

        //Extract the relevant data:
        uint8_t title_set_number       = (uint32_t)title_info.title_set_nr;
        uint8_t title_set_title_number = (uint8_t)title_info.vts_ttn;
        uint16_t number_of_chapters    = (uint16_t)title_info.nr_of_ptts;

        ifoClose(ifo_handle);

        auto title_set_ifo_handle = ifoOpen(handle, title_set_number);
        if (title_set_ifo_handle == nullptr) {
            std::cerr << "Cannot read title set information." << std::endl;
            return EXIT_FAILURE;
        }
        ifoRead_PGCIT(title_set_ifo_handle);

        if (title_set_ifo_handle->vts_pgcit == nullptr) {
            std::cerr << "Cannot get the chapter information for the title." << std::endl;
            return EXIT_FAILURE;
        }
        if (title_number > title_set_ifo_handle->vts_pgcit->nr_of_pgci_srp) {
            std::cerr << fmt::format(
                "The title {0} is out of the range for chapter information. There are only {1} chapter information entries on the medium.",
                title_number,
                (uint16_t)title_set_ifo_handle->vts_pgcit->nr_of_pgci_srp
                ) << std::endl;
            return EXIT_FAILURE;
        }

        //Get the start and end sector for the title by looking at the
        //program chain entry for the title:
        auto title_program_data = title_set_ifo_handle->vts_pgcit->pgci_srp[title_number - 1];
        if (title_program_data.pgc == nullptr) {
            std::cerr << "Cannot get the start and end sector of the title." << std::endl;
            return EXIT_FAILURE;
        }

        if (title_program_data.pgc == nullptr) {
            std::cerr << "The DVD title has no cells." << std::endl;
            return EXIT_FAILURE;
        }
        auto title_data = title_program_data.pgc;
        if (title_data->nr_of_cells < 1) {
            std::cerr << "The DVD title has no cells." << std::endl;
            return EXIT_FAILURE;
        }

        uint32_t first_sector = (uint32_t)title_data->cell_playback[0].first_sector;
        uint32_t last_sector  = (uint32_t)title_data->cell_playback[title_data->nr_of_cells - 1].last_sector;
        uint32_t title_size   = last_sector - first_sector;

        //Open the title set file (one of the "VTS" files):
        auto file = DVDOpenFile(handle, title_set_number, DVD_READ_TITLE_VOBS);
        if (file == 0) {
            std::cerr << fmt::format("Cannot read title set {0}.", title_set_number) << std::endl;
            return EXIT_FAILURE;
        }

        //Display title information:
        std::cerr << fmt::format(
            "Title {0}:\n"
            "\tTitle set: {1}\n"
            "\tTitle set title number: {2}\n"
            "\tChapters: {3}\n"
            "\tTotal size (blocks): {4}\n"
            "\tFirst sector (block): {5}\n"
            "\tLast sector (block): {6}\n"
            "\n",
            title_number,
            title_set_number,
            title_set_title_number,
            number_of_chapters,
            title_size,
            first_sector,
            last_sector
            ) << std::endl;

        FILE* output_file = fopen(output_file_path.c_str(), "wb");
        if (output_file == NULL) {
            std::cerr << "Cannot open the output file." << std::endl;
            return EXIT_FAILURE;
        }

        constexpr size_t block_buffer_size = 32; //Read 32 blocks (32 * 2048 byte) at once.
        constexpr size_t buffer_size = block_buffer_size * DVD_VIDEO_LB_LEN;

        unsigned char* buffer = new unsigned char[buffer_size];
        if (buffer == nullptr) {
            std::cerr << "Cannot allocate buffer." << std::endl;
            return EXIT_FAILURE;
        }

        uint64_t blocks_to_read  = block_buffer_size;

        /**
         * @var next_read_start From where (which block/sector)
         * to start reading.
         */
        uint32_t next_read_start = first_sector;

        std::cerr << std::endl;

        uint8_t i = 0;

        while (auto blocks_read = DVDReadBlocks(file, next_read_start, block_buffer_size, buffer) != -1 && blocks_to_read < title_size) {
            next_read_start += blocks_read;
            size_t write_size = blocks_read * DVD_VIDEO_LB_LEN;
            if (write_size > buffer_size) {
                write_size = buffer_size;
            }
            //Write the data from the buffer:
            fwrite(buffer, 1, write_size, output_file);
            //Adjust the amount of blocks left to read in case there are only
            //a few blocks left to read:
            if (next_read_start + blocks_to_read >= last_sector) {
                blocks_to_read = last_sector - next_read_start - 1;
            }

            if ((i % 200 == 0) || (blocks_to_read < block_buffer_size)) {
                uint32_t blocks_read = next_read_start - first_sector;
                float percentage = ((float)blocks_read / (float)title_size) * 100;
                std::cerr << "\r" << fmt::format(
                    "Reading: {0}/{1} blocks ({2:2.1f}%)",
                    blocks_read,
                    title_size,
                    percentage
                    );
                i = 0;
            } else {
                i++;
            }
        }
        delete buffer;
        fclose(output_file);
        ifoClose(title_set_ifo_handle);
        DVDCloseFile(file);
        DVDClose(handle);
        std::cerr << "\nFinished." << std::endl;
    } else if (args[0] == "list") {
        std::string dvd_device = "/dev/sr0";
        if (args.size() >= 2) {
            //Set dvd device:
            dvd_device = args[1];
        }
        auto handle = DVDOpen(dvd_device.c_str());
        if (handle == 0) {
            std::cerr << "Cannot open the DVD device." << std::endl;
            return EXIT_FAILURE;
        }
        auto ifo_handle = ifoOpen(handle, 0);
        if (ifo_handle == nullptr) {
            std::cerr << "Cannot read title information." << std::endl;
            return EXIT_FAILURE;
        }
        //Read data from the IFO video manager file:

        if (ifo_handle->tt_srpt == nullptr) {
            std::cerr << "Cannot read title information." << std::endl;
            return EXIT_FAILURE;
        }
        if (ifo_handle->tt_srpt->nr_of_srpts < 1
            || ifo_handle->tt_srpt->title == nullptr) {
            std::cerr << "This DVD has no titles." << std::endl;
            return EXIT_FAILURE;
        }

        /*
        //Read the cell address table for the title set:
        ifoRead_TITLE_C_ADT(ifo_handle);
        if (ifo_handle->vts_c_adt == nullptr) {
            //Read failure.
            std::cerr << "Cannot read cell address table for the video title set." << std::endl;
            return EXIT_FAILURE;
        }
        */

        //Get the title set information:
        std::cout << fmt::format(
            "Titles: {0}\n"
            "Title information:\n\n",
            (uint16_t)ifo_handle->tt_srpt->nr_of_srpts
            );
        for (uint16_t i = 0; i < ifo_handle->tt_srpt->nr_of_srpts; i++ ) {
            //The following could be dangerous (reading beyond a limit if nr_of_srpts is bigger than the memory the title pointer points to):
            auto title_info = ifo_handle->tt_srpt->title[i];

            std::cout << fmt::format(
                "Title {1}\n"
                "\tNumber of parts: {2}\n"
                "\tTitle set number: {3}\n"
                "\tTitle number in title set: {4}\n"
                "\tTitle set sector: {5}\n",
                "\t"
                "\n",
                i + 1,
                (uint16_t)title_info.nr_of_ptts,
                (uint32_t)title_info.title_set_nr,
                (uint8_t)title_info.vts_ttn,
                (uint32_t)title_info.title_set_sector
                );
        }

        if (ifo_handle->vts_ptt_srpt == nullptr) {
            std::cout << "No Part-of-title search pointer table.\n\n";
        } else {
            std::cout << fmt::format(
                "Part-of-title search pointer table:\n"
                "Number of entries: {0}:\n\n",
                (uint16_t)ifo_handle->vts_ptt_srpt->nr_of_srpts
                );
        }
        ifoClose(ifo_handle);
        DVDClose(handle);
        std::cerr << "\nFinished." << std::endl;
    } else if (args[0] == "--check") {
        //This is just for compatibility with the moeripper.rb script.
        return EXIT_SUCCESS;
    } else {
        std::cerr << "Not implemented yet." << std::endl;
    }
    return EXIT_SUCCESS;
}
