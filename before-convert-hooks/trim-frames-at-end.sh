#!/bin/bash

frames_to_cut=1
original_file="$PWD/$1/dvd.mpg"
tmp_file="$PWD/$1/dvd.tmp.mpg"

ffmpeg -i "$original_file" -codec:v copy -codec:a copy -map 0 -map -0:s -y "$tmp_file"

frames=$( ffprobe -i "$tmp_file" -select_streams v:0 -count_frames -show_entries stream=nb_read_frames -of flat -v 0 | cut -d= -f2 | tr -d \" )

cut_frames=$( echo "$frames - $frames_to_cut" | bc )
if [ $? -ne 0 ]
then
    echo "Frame calculation error!"
    exit 1
fi
ffmpeg -i "$tmp_file" -codec:v copy -frames:v "$cut_frames" -codec:a copy -map 0 -y "$original_file"
if [ $? -ne 0 ]
then
    echo "FFmpeg trim error!"
    exit 1
fi
rm "$tmp_file"
