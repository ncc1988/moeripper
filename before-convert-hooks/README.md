# before-convert-hooks

A collection of scripts that can be used as before-convert hooks.

## trim-frames-at-end.sh

This script trims frames from the end of the DVD track. It is useful
when the DVD track has DVD menu images at the end that do not belong
to the main content of the track.
