#!/usr/bin/env ruby
#encoding: utf-8


#
#    moeripper.rb,
#    Copyright (C) 2014-2020  Moritz Strohm <ncc1988@posteo.de>
#
#    This file is part of moeripper - a dvd ripping solution
#
#    moeripper is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    moeripper is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with moeripper.  If not, see <http://www.gnu.org/licenses/>.
#


#this program requires the following software packages and programs:
# - mpv
# - ffmpeg, with libtheora and libvorbis support
# - normalize / normalize-audio
# - vorbisgain (and at your option mp3gain)
# - mkvmerge (mkvtoolnix)
# - moeripper_cpp (can be compiled using make in the source code directory where this file lies in)
#
#You can test if these programs are installed by calling moeripper.rb with the parameter checkinstall:
# ./moeripper.rb checkinstall


require 'open3'


MOERIPPER_VERSION = "0.3.1"
PROFILE_FILE_VERSION = "3"

PROFILE_NAME = 0
PROFILE_DESC = 1
PROFILE_PROCESSING = 2
PROFILE_VCONFIG = 3
PROFILE_ACONFIG = 4

VCONFIG_RESOLUTION = 0
VCONFIG_FPS = 1
VCONFIG_DEINTERLACE = 2 # in String: p = progressive (deinterlace), i = leave interlaced, s = swap fields before deinterlacing
VCONFIG_CODEC = 3
VCONFIG_QUALITY = 4
VCONFIG_FOURCC = 5

ACONFIG_RATE = 0
ACONFIG_CHANNELS = 1
ACONFIG_CODEC = 2
ACONFIG_QUALITY = 3

JOB_MODE = 0
JOB_PROFILE = 1
JOB_DVDNAME = 2
JOB_DVDTRACK = 3
JOB_TITLE = 4
JOB_INDEX = 5
JOB_VASPECT = 6
JOB_AORDER = 7


$useNormalize = true #if set to true, the normalize executable will be used. Otherwise normalize-audio will be used.

class ProcessingConfig
    attr_accessor(:two_pass)
    def initialize(config_string = nil)
        @two_pass = false
        if (config_string == 'v2')
            @two_pass = true
        end
    end


    def toProfileString()
        if (@two_pass)
            return 'v2'
        else
            return 'v1'
        end
    end
end


class VideoConfig
    attr_accessor(:resolution, :fps, :deinterlace, :codec, :qualityMode, :qualityParam, :fourCC)
    def initialize(videoConfigString = nil)
        if(videoConfigString != nil)
            videoConfigString = videoConfigString.split(":")
            @resolution = videoConfigString[VCONFIG_RESOLUTION]
            @fps = videoConfigString[VCONFIG_FPS]
            @deinterlace = videoConfigString[VCONFIG_DEINTERLACE]
            @codec = videoConfigString[VCONFIG_CODEC]
            quality = videoConfigString[VCONFIG_QUALITY].split("@")
            @qualityMode = quality[0] #mode: b for bitrate, q for quality
            @qualityParam = quality[1] #bitrate in bit/s or quality level
            @fourCC = videoConfigString[VCONFIG_FOURCC].to_s() #this is an optional parameter so it might be converted from nil to string
        else
            @resolution = "0x0"
            @fps = "0"
            @deinterlace = "p"
            @codec = "null"
            @qualityMode = "q"
            @qualityParam = "0"
            @fourCC = ""
        end
    end

    def toProfileString()
        return @resolution+":"+@fps+":"+@deinterlace+":"+@codec+":"+@qualityMode+"@"+@qualityParam+":"+@fourCC
    end
end


class AudioConfig
    attr_accessor(:rate, :channels, :codec, :qualityMode, :qualityParam)
    def initialize(audioConfigString = nil)
        if(audioConfigString != nil)
            audioConfigString = audioConfigString.split(":")
            @rate = audioConfigString[ACONFIG_RATE]
            @channels = audioConfigString[ACONFIG_CHANNELS]
            @codec = audioConfigString[ACONFIG_CODEC]
            quality = audioConfigString[ACONFIG_QUALITY].split("@")
            @qualityMode = quality[0]
            @qualityParam = quality[1]
        else
            @rate = "0"
            @channels = "0"
            @codec = "null"
            @qualityMode = "q"
            @qualityParam = "0"
        end
    end

    def toProfileString
        return @rate+":"+@channels+":"+@codec+":"+@qualityMode+"@"+@qualityParam
    end
end

class Profile
    attr_accessor(:name, :description, :processing_config, :videoConfig, :audioConfig)
    def initialize(profileLine = nil)
        if(profileLine != nil)
            profileLine = profileLine.split(";")
            #DEBUG:
            #puts "DEBUG: "+profileLine.to_s()
            #PROFILE_ACONFIG is the last column of the profile line
            if(profileLine.length != PROFILE_ACONFIG+1)
                puts "ERROR: read invalid profile line in Profiles.csv!"
                raise Exception("invalid profile line!")
            else
                #everything went fine: set own attributes:
                @name = profileLine[PROFILE_NAME]
                @description = profileLine[PROFILE_DESC]
                @processing_config = ProcessingConfig.new(profileLine[PROFILE_PROCESSING])
                @videoConfig = Array.new()
                @audioConfig = Array.new()
                videoConfigArray = profileLine[PROFILE_VCONFIG].split(",")
                audioConfigArray = profileLine[PROFILE_ACONFIG].split(",")
                videoConfigArray.each do |vc|
                    @videoConfig.push(VideoConfig.new(vc))
                end

                audioConfigArray.each do |ac|
                    @audioConfig.push(AudioConfig.new(ac))
                end
            end
        else
            @name = ''
            @description = ''
            @processing_config = ProcessingConfig.new()
            @videoConfig = Array.new()
            @audioConfig = Array.new()
        end
    end

    def toProfileString()
        profileString = @name + ';' + @description + ';' + @processing_config.toProfileString() + ';'
        i = 0
        @videoConfig.each do |v|
            if (i > 0)
                profileString += ","
            end
            profileString += v.toProfileString()
            i += 1
        end
        profileString += ";"
        i = 0
        @audioConfig.each do |a|
            if (i > 0)
                profileString += ","
            end
            profileString += a.toProfileString()
            i += 1
        end
        profileString += ";"
        return profileString
    end
end


##
# The Moeripper class is the main class.
class Moeripper

    ##
    # Prints out help text.
    def self.showHelp()
        puts
        puts "Usage: moeripper.rb OPTION"
        puts "  where OPTION is one of the following:"
        puts
        puts "  convertprofiles"
        puts "   – converts old profile files to the current version"
        puts
        puts "  checkinstall"
        puts "   – checks if all necessary programs are installed"
        puts
        puts "  help"
        puts "   – displays this help"
        puts
        puts "  version"
        puts "   – outputs the version number of moeripper"
        puts
        puts "  wizard"
        puts "   – starts a wizard to ease the process of adding ripping jobs to the joblist"
        puts
        puts
    end


    ##
    # Checks if the necessary programs are installed.
    def self.checkInstallation()
        system("mpv -v > /dev/null 2>&1")
        if($? == 0)
            puts "mpv is\tinstalled!"
        else
            puts "mpv is\tnot installed!"
        end

        system("ffmpeg -version > /dev/null 2>&1")
        if($? == 0)
            puts "ffmpeg is\tinstalled!"
        else
            puts "ffmpeg is\tnot installed!"
        end

        system("normalize --version > /dev/null 2>&1")
        if($? == 0)
            puts "normalize is\tinstalled!"
        else
            #for Debian GNU/Linux and derivatives:
            system("normalize-audio --version > /dev/null 2>&1")
            if($? == 0)
                puts "normalize is\tinstalled! (as normalize-audio)"
            else
                puts "normalize is\tnot installed! (or normalize-audio is not installed)"
            end
        end

        system("vorbisgain --version > /dev/null 2>&1")
        if($? == 0)
            puts "vorbisgain is\tinstalled!"
        else
            puts "vorbisgain is\tnot installed!"
        end

        system("mp3gain -v > /dev/null 2>&1")
        if($? == 0)
            puts "mp3gain is\tinstalled!"
        else
            puts "mp3gain is\tnot installed!"
        end

        system("mkvmerge --version > /dev/null 2>&1")
        if($? == 0)
            puts "mkvmerge is\tinstalled!"
        else
            puts "mkvmerge is\tnot installed!"
        end

        system("moeripper_cpp --check")
        if($? == 0)
           puts "moeripper_cpp is \tinstalled"
        else
           puts "moeripper_cpp is \tnot installed."
           puts "Please run make and copy the moeripper_cpp executable in a path for executable programs (~/bin for example)."
        end
    end


    ##
    # Parses DVD audio stream information captured from the output of
    # the ripping program.
    #
    # @param String captured_audio_info Captured audio stream information.
    #
    # @param String format The format of captured_audio_info. At the moment,
    #     only the output of mpv is supported, so this value has to be "mpv".
    #
    # @returns Hash A hash with the audio stream ID as index and
    #     a second hash as "value". The second dictionary has the
    #     :lang index set to a string representing a two-digit language code.
    def self.parseDvdAudioStreamInfo(captured_audio_info = '', format = 'mpv')
        if (format != 'mpv')
            return {}
        end

        if (!captured_audio_info)
            return {}
        end

        stream_infos = {}
        audio_info = captured_audio_info.lines()
        audio_info.each { |line|
            audio_id, status = Open3.capture2('cut -d\' \' -f1', :stdin_data => line)
            audio_id.chomp!('')
            lang_code, status = Open3.capture2('cut -d= -f2 | cut -d\' \' -f1', :stdin_data => line)
            lang_code.chomp!('')
            if (lang_code)
                stream_infos[audio_id] = {
                    :lang => lang_code
                }
            else
                stream_infos[audio_id] = {
                    :lang => 'un'
                }
            end
        }
        return stream_infos
    end

    ##
    # Outputs the DVD audio stream info to a text file.
    def self.writeDvdAudioStreamInfo(audio_info = {})
        filename = './wizard_atracks.txt'
        file = File.new(filename, 'w')
        audio_info.each { |stream_id, stream_info|
            file.puts(stream_id + '=' + stream_info[:lang])
        }
        file.close()
        return nil
    end

    ##
    # Reads the DVD audio stream info from a text file.
    #
    # @return Hash A hash containing the audio stream information.
    def self.readDvdAudioStreamInfo()
        filename = './wizard_atracks.txt'
        file = File.new(filename, 'r')
        audio_info = {}
        file.each_line { |line|
            line = line.split('=')
            stream_id = line[0]
            lang_id = line[1]
            if (stream_id)
                if (lang_id)
                    audio_info[stream_id] = {
                        :lang => lang_id
                    }
                else
                    audio_info[stream_id] = {
                        :lang => 'un'
                    }
                end
            end
        }
        file.close()
        return audio_info
    end


    ##
    # Reads the audio stream information of the dvd file using ffmpeg.
    # The information is added to the AORDER hash of the current job.
    def self.readFfmpegAudioStreamInfo(dvd_file)
        ffmpeg_audio_info = `ffmpeg -i "#{dvd_file}" 2>&1 | grep 'Stream.*Audio'`

        #split the audio stream information per line:
        ffmpeg_audio_info = ffmpeg_audio_info.split("\n")

        ffmpeg_audio_info.each { |stream_line|
            stream_line_part = stream_line.gsub(" ","").gsub("Stream#0:","").split(":")[0]
            #get the ffmpeg stream id as decimal
            ffmpeg_audio_id = Integer(stream_line_part.split("[")[1].gsub("]",""))
            #and the ffmpeg audio channel id:
            ffmpeg_stream_id = stream_line_part.split("[")[0]

            #DVD streams start with an ID of 0x80 in ffmpeg.
            #Since mpv starts counting stream-IDs from 1,
            #we have to substract 0x7f from the stream-ID to
            #map ffmpeg stream-IDs to mpv stream-IDs.
            current_audio_id = (ffmpeg_audio_id - 0x7f).to_s()

            if ($CurrentJob[JOB_AORDER][current_audio_id])
                #The stream-ID belongs to a language that shall be ripped.
                #Add the ffmpeg stream data to the job aorder hash.
                $CurrentJob[JOB_AORDER][current_audio_id][:ffmpeg_stream_id] = ffmpeg_stream_id.to_s()
            end
        }
    end


    ##
    # Converts a job hash to a text line representing the job
    #
    # @param Hash job The job hash to be converted.
    #
    # @returns String A CSV-like text line representing the job.
    def self.createJobLine(job)
        currentJobLine = [
            job[JOB_MODE].join(""),
            job[JOB_PROFILE].name,
            job[JOB_DVDNAME],
            job[JOB_DVDTRACK],
            job[JOB_TITLE],
            job[JOB_INDEX],
            job[JOB_VASPECT]
        ]
        aorder = []
        job[JOB_AORDER].each { |stream_id, stream_data|
            aorder_text = stream_id + '=' + stream_data[:lang]
            if (stream_data[:ffmpeg_stream_id])
                aorder_text += '=' + stream_data[:ffmpeg_stream_id]
            end
            aorder.push(aorder_text)
        }
        currentJobLine.push(aorder.join(','))
        return currentJobLine.join(';')
    end


    ##
    # Builds data structures from the current job text line.
    def self.preprocessCurrentJob()
        if(($CurrentJob[JOB_MODE].length > 2) || ($CurrentJob[JOB_MODE] == ""))
            return false
        else
            $CurrentJob[JOB_MODE] = $CurrentJob[JOB_MODE].split(//)
        end
        if($CurrentJob[JOB_PROFILE] == "")
            return false
        else
            i = 0
            match_c = 0
            jobProfile = -1
            $Profiles.each do |p|
                if(p.name == $CurrentJob[JOB_PROFILE])
                    jobProfile = i
                    match_c += 1
                end
                i += 1
            end
            if(match_c == 0)
                puts "ERROR: No known profile named "+$CurrentJob[JOB_PROFILE]
                return false
            elsif(match_c > 1)
                puts "ERROR: Profile named "+$CurrentJob[JOB_PROFILE]+ " is ambiguous"
                return false
            else
                #set the job profile for the current job:
                $CurrentJob[JOB_PROFILE] = $Profiles[jobProfile]
                #DEBUG:
                #puts "DEBUG: JOB_PROFILE="+$CurrentJob[JOB_PROFILE].to_s()
            end
        end
        if($CurrentJob[JOB_DVDNAME] == "")
            return false
        end
        if($CurrentJob[JOB_DVDTRACK] == "")
            return false
        end
        if($CurrentJob[JOB_TITLE] == "")
            return false
        end
        #the job index may be empty, so no checking done here
        if($CurrentJob[JOB_VASPECT] == "")
            return false
        else
            #do nothing

            #$CurrentJob[JOB_VASPECT] = $CurrentJob[JOB_VASPECT].split(",")
            #if($CurrentJob[JOB_VASPECT].length < 1)
            #  puts "ERROR: video configuration is invalid!"
            #  return false
            #end
        end
        if ($CurrentJob[JOB_AORDER] == "")
            return false
        else
            #aorder format description:
            #<mpv stream ID>=<lang ID>[=<ffmpeg stream ID>]
            #ffmpeg stream ID is optional.
            aorder_strings = $CurrentJob[JOB_AORDER].split(",")
            $CurrentJob[JOB_AORDER] = {}
            aorder_strings.each { |a_string|
                a_parts = a_string.split('=')
                if (a_parts.length() >= 2)
                    stream_id = a_parts[0]
                    lang_id = a_parts[1]
                    valid_language_code = checkLanguageSupport(lang_id)
                    if (valid_language_code == false)
                        puts 'ERROR: unsupported language code "' + lang_id + '" in audio configuration!'
                        return false
                    end

                    data = {
                        :lang => lang_id
                    }
                    if (a_parts.length() >= 3)
                        data[:ffmpeg_stream_id] = a_parts[2]
                    end
                    $CurrentJob[JOB_AORDER][stream_id] = data
                end
            }
        end
        return true
    end


    ##
    # Rips the current job.
    def self.ripCurrentJob()

        if (!$CurrentJob[JOB_AORDER])
            puts 'ERROR: No audio stream configuration specified! Cannot start ripping!'
            exit 1
        end

        puts "----------------"
        puts "Ripping..."
        puts "----------------"

        dvddir = getCurrentJobDirectory()

        #create directory for the current job:
        system("mkdir \"./#{dvddir}/\" > /dev/null 2>&1")

        #start mpv:
        system("moeripper_cpp rip #{$CurrentJob[JOB_DVDTRACK]} #{dvddir}/dvd.mpg")
        if($? != 0)
            puts "Error: moeripper_cpp error! Abort!"
            exit 1
        end

        #PROBLEM at audio order processing:
        #What happens when an unwanted audio stream
        #is between two desired audio streams?
        #(e.g. a second english audio track
        #with director's comments between
        #the english and german audio track)

        #we're ready to convert
        puts "----------------"
        puts "Done ripping!"
        puts "----------------"
        return true
    end
end


CONFIGDIR = Dir.home()+"/.config/moeripper/"
FILE_PROFILES = CONFIGDIR + "Profiles.csv"

FILE_TODO = "moeripper.todo.csv"
FILE_TODO_OLD = "moeripper.todo"
FILE_DONE = "moeripper.done.csv"

$Profiles = Array.new

$CurrentJob = []

$jobsProcessed = 0 #the amount of jobs that were processed by this run of moeripper

system("mkdir -p "+CONFIGDIR+" 2> /dev/null") #create the config directory if not existing


def addProfile(profileString)
    profile = Profile.new(profileString)
    #puts "DEBUG: AddProfile: "+profile.toProfileString()
    $Profiles.push(profile)
end


##reads the conversion profile file:
def readProfiles()
    if(FileTest.exists?(FILE_PROFILES))
        profilesFile = File.open(FILE_PROFILES, "r")
        #read file version:
        profileFileVersion = profilesFile.gets.gsub("\n","")
        if(profileFileVersion == "@v"+PROFILE_FILE_VERSION)
            #supported version: read profiles
            profilesFile.gets #jump over table description line
            profileDefinition = ""

            while((profileDefinition = profilesFile.gets) != nil)
                addProfile(profileDefinition.gsub("\n",""))
            end
        #all profiles read here
        else
            if(profileFileVersion == "@v0" or profileFileVersion == "@v1")
                puts "ERROR: Your profiles file is outdated! Please run \"moeripper convertprofiles\" to correct this!"
            else
                puts "ERROR: Profiles file is in an unsupported version!"
            end
        end
    else
        puts "ERROR: No profiles file given! Cannot continue!\n"
        puts "Please create a profile by hand or by using moeripper-profiles.rb\n"
        puts "The profiles file should be "+FILE_PROFILES
    end
end


def convertProfileFile()
    newProfileContent = ""
    if(FileTest.exists?(FILE_PROFILES))
        profilesFile = File.open(FILE_PROFILES, "r")
        #read file version:
        profileFileVersion = profilesFile.gets.gsub("\n","")
        if(profileFileVersion == "@v0")
            profilesFile.gets #jump over table description line

            #supported old version: read profiles and add the profile description field

            newProfileContent = "@v"+PROFILE_FILE_VERSION+"\n"
            newProfileContent += "ProfileName;ProfileDescription;VideoResolution;VideoFPS;VideoFrameConfig;VideoCodec;VideoQuality;VideoFourCC;AudioSamplerate;AudioChannels;AudioCodec;AudioQuality;\n"

            profileDefinition = ""

            #v0 format: ProfileName;VideoResolution;VideoFPS;VideoFrameConfig;VideoCodec;VideoQuality;VideoFourCC;AudioSamplerate;AudioChannels;AudioCodec;AudioQuality;
            while((oldProfile = profilesFile.gets) != nil)
                oldProfile = oldProfile.gsub("\n","").split(";")
                profile = Profile.new()
                profile.name = oldProfile[0]
                vconfig = VideoConfig.new()
                vconfig.resolution = oldProfile[1]
                vconfig.fps = oldProfile[2]
                vconfig.deinterlace = oldProfile[3]
                vconfig.codec = oldProfile[4]
                vq = oldProfile[5].split(",")
                vconfig.qualityMode = vq[0]
                vconfig.qualityParam = vq[1]
                vconfig.fourCC = oldProfile[6]
                profile.videoConfig.push(vconfig)
                aconfig = AudioConfig.new()
                aconfig.rate = oldProfile[7]
                aconfig.channels = oldProfile[8]
                aconfig.codec = oldProfile[9]
                aq = oldProfile[10].split(",")
                aconfig.qualityMode = aq[0]
                aconfig.qualityParam = aq[1]
                profile.audioConfig.push(aconfig)
                puts "DEBUG: Converted v0 profile: "+profile.toProfileString()
                newProfileContent += profile.toProfileString() + "\n"
            end
            profilesFile.close()
            system("mv \""+FILE_PROFILES+"\" \""+FILE_PROFILES+".bak\"")
            profilesFile = File.open(FILE_PROFILES, "w")
            profilesFile.puts newProfileContent
            puts "Conversion of old profiles file (v0 -> v"+PROFILE_FILE_VERSION+") finished!"

        elsif(profileFileVersion == "@v1")
            profilesFile.gets #jump over table description line

            #supported old version: read profiles and add the profile description field

            newProfileContent = "@v"+PROFILE_FILE_VERSION+"\n"
            newProfileContent += "ProfileName;ProfileDescription;VideoResolution;VideoFPS;VideoFrameConfig;VideoCodec;VideoQuality;VideoFourCC;AudioSamplerate;AudioChannels;AudioCodec;AudioQuality;\n"

            profileDefinition = ""

            #v0 format: ProfileName;VideoResolution;VideoFPS;VideoFrameConfig;VideoCodec;VideoQuality;VideoFourCC;AudioSamplerate;AudioChannels;AudioCodec;AudioQuality;
            while((oldProfile = profilesFile.gets) != nil)
                oldProfile = oldProfile.gsub("\n","").split(";")
                profile = Profile.new()
                profile.name = oldProfile[0]
                profile.description = oldProfile[1]
                vconfig = VideoConfig.new()
                vconfig.resolution = oldProfile[2]
                vconfig.fps = oldProfile[3]
                vconfig.deinterlace = oldProfile[4]
                vconfig.codec = oldProfile[5]
                vq = oldProfile[6].split(",")
                vconfig.qualityMode = vq[0]
                vconfig.qualityParam = vq[1]
                vconfig.fourCC = oldProfile[7]
                profile.videoConfig.push(vconfig)
                aconfig = AudioConfig.new()
                aconfig.rate = oldProfile[8]
                aconfig.channels = oldProfile[9]
                aconfig.codec = oldProfile[10]
                aq = oldProfile[11].split(",")
                aconfig.qualityMode = aq[0]
                aconfig.qualityParam = aq[1]
                profile.audioConfig.push(aconfig)
                puts "DEBUG: Converted v0 profile: "+profile.toProfileString()
                newProfileContent += profile.toProfileString() + "\n"
            end
            profilesFile.close()
            system("mv \""+FILE_PROFILES+"\" \""+FILE_PROFILES+".bak\"")
            profilesFile = File.open(FILE_PROFILES, "w")
            profilesFile.puts newProfileContent
            puts "Conversion of old profiles file (v1 -> v"+PROFILE_FILE_VERSION+") finished!"
        elsif (profileFileVersion == '@v2')
            profilesFile.gets #jump over table description line

            newProfileContent = "@v"+PROFILE_FILE_VERSION+"\n"
            newProfileContent += "Profile name;Profile description;Processing configuration;Video configurations;Audio configurations;\n"
            while ((oldProfile = profilesFile.gets) != nil)
                oldProfile = oldProfile.gsub("\n",'').split(';')
                profile = Profile.new()
                profile.name = oldProfile[0]
                profile.description = oldProfile[1]
                vconfig_parts = oldProfile[2].split(',')
                aconfig_parts = oldProfile[3].split(',')
                vconfigs = Array.new()
                aconfigs = Array.new()
                vconfig_parts.each do |vcpart|
                    profile.videoConfig.push(VideoConfig.new(vcpart))
                end
                aconfig_parts.each do |acpart|
                    profile.audioConfig.push(AudioConfig.new(acpart))
                end

                puts "DEBUG: Converted v2 profile: "+profile.toProfileString()
                newProfileContent += profile.toProfileString() + "\n"
            end
            profilesFile.close()
            system("mv \""+FILE_PROFILES+"\" \""+FILE_PROFILES+".bak\"")
            profilesFile = File.open(FILE_PROFILES, "w")
            profilesFile.puts newProfileContent
            puts "Conversion of old profiles file (v2 -> v"+PROFILE_FILE_VERSION+") finished!"
            profileDefinition = ""
        elsif (profileFileVersion == '@v3')
            puts "The Profiles file is already in the current format!"
        else
            puts "ERROR: Profiles file is in an unsupported version and cannot be converted!"
        end
    else
        puts "ERROR: No profiles file given! Cannot continue!\n"
        puts "Please create a profile by hand or by using moeripper-profiles.rb\n"
        puts "The profiles file should be "+FILE_PROFILES
    end

end



def getCurrentJobDirectory()
    return $CurrentJob[JOB_DVDNAME] + "-" + $CurrentJob[JOB_DVDTRACK]
end


$LanguageCodeMap =
    [
        ["unknown", "und"], #undefined language

        ["de","ger"],
        ["en","eng"],
        ["fr","fre"],
        ["es","spa"],
        ["it","ita"],
        ["ja","jpn"],
        ["ru","rus"],
        ["cs","cze"],
        ["pl","pol"],
        ["hu","hun"],
        ["pt","por"]
    ]


def convertLanguageCode(l)
    for lc in $LanguageCodeMap
        if lc[0] == l
            return lc[1]
        end
    end
    #we have reached the end of the loop without a match:
    #return undefined language code.
    return 'und'
end


def checkLanguageSupport(l) #returns true if supported, false if not
    return (convertLanguageCode(l) != nil)
end




def jobWizard()
    puts "Welcome to the moeripper job wizard."
    puts "This will help you to create moeripper jobs."
    puts "You can always abort the wizard by pressing CRTL-C.\n\n"

    dvd_audio_config = {}
    newJob = []

    choiceOK = []
    choiceOK[0] = false
    while(choiceOK[0] == false)

        puts
        puts "Step 1/8: Mode selection"
        puts "------------------------"
        puts "In this step we will define what moeripper shall do with this job."
        puts "The available choices are: rip only (r) or only convert the video (c)."
        puts "Both can be combined: rip and convert (rc)"
        puts "Usually one chooses rip (r) or rip and convert (rc)."
        puts "Convert only (c) is only useful when you previously ripped a DVD."
        puts ""
        puts "Make your choice [r/c/rc]:"
        choice = $stdin.gets.gsub("\n","").upcase()
        #choice="RC" #development only

        if(choice == "R")
            puts "You have selected rip only (r)."
            newJob[JOB_MODE] = "r"
            choiceOK[0] = true
        elsif(choice == "C")
            puts "You have selected convert (c)."
            choice2 = ""
            puts "WARNING: If you didn't rip the video moeripper will fail when it tries to convert it!"

            choiceOK[1] = false
            while(choiceOK[1] == false)
                puts "Do you want to rip the video first?[y/n]"
                choice2 = $stdin.gets.gsub("\n","").upcase()
                if(choice2 == "Y")
                    newJob[JOB_MODE] = "rc"
                    choiceOK[1] = true
                elsif(choice2 == "N")
                    newJob[JOB_MODE] = "c"
                    choiceOK[1] = true
                end
            end

            choiceOK[0] = true

        elsif(choice == "RC")
            puts "You have selected rip and convert (rc)"
            newJob[JOB_MODE] = "rc"
            choiceOK[0] = true
        else
            puts "You have entered an invalid choice. Please try again!"
        end
    end

    puts "Moeripper can pause at the end of a job."
    puts "This means you have to press enter to let moeripper continue with the next job after it completed a job."
    puts "It can be useful if you need to change the DVD in the drive."

    choiceOK[0] = false
    while(choiceOK[0] == false)

        puts "Would you like moeripper to pause after this job was finished?[y/n]"
        choice = $stdin.gets.gsub("\n","").upcase()
        #choice = "Y" #development only
        if(choice == "Y")
            newJob[JOB_MODE].capitalize!()
            newJob[JOB_MODE] = newJob[JOB_MODE].split(//)
            choiceOK[0] = true
        elsif(choice == "N")
            newJob[JOB_MODE].upcase!()
            newJob[JOB_MODE] = newJob[JOB_MODE].split(//)
            choiceOK[0] = true
        end
    end

    puts
    puts "Step 2/8: Profile selection"
    puts "---------------------------"
    puts "The following profiles are available:"
    for i in (0..$Profiles.length-1)
        puts i.to_s() + ": " + $Profiles[i].name + " – " + $Profiles[i].description
    end
    i = $Profiles.length

    choice = ""
    choiceOK[0] = false

    while(choiceOK[0] == false)
        puts "Choose a profile [0 - "+(i-1).to_s()+"]"
        choice = $stdin.gets.gsub("\n","").to_i()
        #choice=0 #development only

        if(choice >= 0 && choice < i)
            newJob[JOB_PROFILE] = $Profiles[choice]
            puts "The chosen profile is: " + newJob[JOB_PROFILE].name
            choiceOK[0] = true
        else
            puts "Invalid profile number!"
        end
    end

    puts
    puts "Step 3/8: dvd name selection"
    puts "----------------------------"
    puts "For easy project identification you need to provide a name for the DVD."
    puts "This name is only used for a project folder inside the current working directory"
    puts "in which all temporary files of the ripping project are stored."
    puts "In the given DVD name blank spaces will be removed."
    puts ""

    choice = ""
    choiceOK[0] = false
    while(choiceOK[0] == false)
        puts "Choose a project name (at least 1 character):"
        choice = $stdin.gets.gsub("\n","").gsub(" ","")
        #choice="test" #development only

        if(choice.length > 0)
            choiceOK[0] = true
            newJob[JOB_DVDNAME] = choice
            puts "DVD project name will be: \"" + choice + "\""
        else
            puts "Name too short!"
        end
    end

    puts
    puts "Step 4/8: DVD track selecting/probing"
    puts "-------------------------------------"
    puts "Now we have to select a DVD track that shall be ripped or converted."
    puts "If you already know what DVD track you wish to rip or convert"
    puts "please enter the DVD track number here. Otherwise type \"p\" for probing."

    choice = ""
    choiceOK[0] = false
    choiceOK[1] = false
    while(choiceOK[0] == false)
        puts "Choose a dvd track [0 - 99 or p]:" #TODO: check maximum number for a DVD track
        choice = $stdin.gets().gsub("\n","")
        #choice="p" #development only

        if(choice == "p")
            puts "Manual probing"
            puts "--------------"
            puts "Mpv will now start playing the DVD from track 1 to the last track."
            puts "While playing you can skip forward and backward with the arrow keys."
            puts "To stop the playback and continue with the wizard press \"q\" in the mpv window."
            puts "After each track you will be asked if the right track was played."
            puts "Answer these questions with y (yes), n (no) or a (abort)."
            puts "If you select \"a\" you will return to the dvd track selection."
            puts "The probing process will start after you press the enter key."
            $stdin.gets()
            i = 1
            while(choiceOK[1] == false)
                mpv_cmd = 'mpv dvdread://' + choice + ' | grep \'Audio --aid\' | cut -d= -f2,3'
                audio_lines, status = Open3.capture2(mpv_cmd)
                dvd_audio_config = Moeripper.parseDvdAudioStreamInfo(audio_lines)
                puts dvd_audio_config.to_s()
                if(status != 0)
                    puts "WARNING: Mpv reported an error! Maybe the last track was reached!"
                end
                puts "Was this the right DVD track?[y/n/a]"
                choice2 = ""
                choice2 = $stdin.gets().gsub("\n","")
                if(choice2 == "y")
                    puts "DVD project will process DVD track " + i.to_s() + "!"
                    newJob[JOB_DVDTRACK] = i.to_s()
                    choiceOK[1] = true
                    choiceOK[0] = true
                elsif(choice2 == "n")
                    choiceOK[1] = false
                    i=i+1
                    puts "The next track (number "+i.to_s()+") will start after you press the enter key."
                    $stdin.gets()
                elsif(choice2 == "a")
                    choiceOK[1] = true
                    puts "The probing process was aborted!"
                else
                    puts "Invalid choice!"
                end
            end

        elsif(choice.to_i() > -1)
            choiceOK[0] = true
            newJob[JOB_DVDTRACK] = choice
            mpv_cmd = 'mpv -no-video -no-audio dvdread://' + choice + ' | grep \'Audio --aid\' | cut -d= -f2,3'
            audio_lines, status = Open3.capture2(mpv_cmd)
            dvd_audio_config = Moeripper.parseDvdAudioStreamInfo(audio_lines)
            puts "DVD project will process DVD track " + choice + "!"
        else
            puts "Invalid track number!"
        end
    end


    puts ''
    puts "Step 5/8: dvd title selection"
    puts "-----------------------------"
    puts "This is the title of the ripped video that will be embedded in the Matroska file after conversion and that will be set for the converted video file name."

    choice = ""
    choiceOK[0] = false
    while(choiceOK[0] == false)
        puts "Choose a video title (at least 1 character):"
        choice = $stdin.gets.gsub("\n","")
        if(choice.length > 0)
            choiceOK[0] = true
            newJob[JOB_TITLE] = choice
            puts "Video title will be: \"" + choice + "\""
        else
            puts "Name too short!"
        end
    end

    puts
    puts "Step 6/8: Video index selection"
    puts "-------------------------------"
    puts "You can enter an index here. This is mostly useful for series to determine"
    puts "what episode of a season the video belongs to."
    puts "Usually the index is written like this: \"6-02\" or \"6x02\" for the second episode of the sixth season"
    puts "You can skip this step by just pressing enter."
    newJob[JOB_INDEX] = $stdin.gets.gsub("\n","")

    puts
    puts "Step 7/8: video configuration"
    puts "-----------------------------"
    puts "In this step you can choose the aspect ratio of a video."
    puts "Usual values are fractions like 4:3 (TV), 16:9 (Widescreen TV) or 2.35 (Cinema) or 1.67."
    puts ""
    puts "Warning: This step is still under development!"
    puts "As of now only the PAL frame rate (25 fps) with progressive (p) frames is supported!"
    puts "For other frame rates and for interlaced video (i or s) you have to edit the job file yourself!"
    puts

    choice = ""
    choiceOK[0] = false
    while(choiceOK[0] == false)
        puts "Choose the video aspect ratio (4:3, 16:9, 2.35, ...):"
        choice = $stdin.gets.gsub("\n","").gsub(" ","")
        if(choice.to_f() > 0.0) #TODO: do a real value check
            choiceOK[0] = true
            newJob[JOB_VASPECT] = []
            newJob[JOB_VASPECT] = choice
            puts "Video aspect ratio will be: \"" + choice + "\""
        else
            puts "Invalid value!"
        end
    end

    audio_languages = {}
    dvd_audio_config.each { |stream_id, audio_stream_info|
        audio_languages[audio_stream_info[:lang]] = stream_id
    }

    puts
    puts "Step 8/8: audio language selection"
    puts "-----------------------------"
    if(audio_languages.length > 0)
        puts "The above selected DVD track contains the following audio languages:"
        audio_languages.each { |lang, data|
            puts lang
        }
    else
        puts "It is unknown which audio languages the above selected DVD track cointains."
        puts "You can simply guess by entering the language(s) you want (e.g. en,de,es,fr)."
    end
    puts "If you want to select all languages of the dvd you have to specify every language the dvd contains."
    puts "The languages will be stored in the order you enter them here."
    choice = ""
    lang_choice = []
    choiceOK[0] = false
    while(choiceOK[0] == false)
        puts "Enter the languages you want to convert, separated by comma (\",\")."
        choice = $stdin.gets.gsub("\n","").gsub(" ","")
        lang_choice = choice.split(",")

        validLanguageCodes = true
        l = 0
        lang_choice.each { |lang|
            if (!checkLanguageSupport(lang))
                validLanguageCodes = false
                break
            end
        }

        if(validLanguageCodes == false)
            puts "ERROR: unsupported language code in audio configuration!"
            return false
        end

        audio_stream_order = {}
        lang_choice.each { |lang|
            if (audio_languages[lang])
                stream_id = audio_languages[lang]
                audio_stream_order[stream_id] = {
                    :lang => lang
                }
            end
        }
        allLangFound = audio_stream_order.length() == lang_choice.length()
        if(allLangFound == true)
            newJob[JOB_AORDER] = audio_stream_order
        else
            puts "You have entered an invalid choice. Please try again!"
        end
        choiceOK[0] = allLangFound
    end

    puts "END OF WIZARD!"
    puts "--------------"
    puts "Congratulations! You have finished the wizard and created a new job for moeripper."
    puts "The following job line will be created in \"" + FILE_TODO + "\":"
    puts
    job_line = Moeripper.createJobLine(newJob)
    puts job_line
    puts
    job_file = File.new(FILE_TODO, 'a')
    job_file.puts(job_line)
    puts "The job line has been written. You can start moeripper now to process the job. Have a nice day!"
end




def createCurrentJobLine()
    return Moeripper.createJobLine($CurrentJob)
end


def updateCurrentJob()
    #NOT WORKING: NOT REVIEWED/REWRITTEN!

    #the current job is always in the second line of the job list
    #(when line numbers start at 1)


    headLine = `head -1 \"#{FILE_TODO}\"`
    currentJobLine = createCurrentJobLine()

    #DEBUG:
    #puts currentJobLine
    otherJobLines = `tail -n +3 \"#{FILE_TODO}\" `

    jobFile = File.open(FILE_TODO, "w")
    jobFile.puts headLine
    jobFile.puts currentJobLine
    jobFile.print otherJobLines #don't create another newline after the last job
    jobFile.close()
end




def finishCurrentJob()
    dvddir = getCurrentJobDirectory()
    if($CurrentJob[JOB_INDEX] == "")
        system("mv \"./#{dvddir}/out.mkv\" \"./#{$CurrentJob[JOB_TITLE]}.mkv\"");
    else
        system("mv \"./#{dvddir}/out.mkv\" \"./#{$CurrentJob[JOB_INDEX]} #{$CurrentJob[JOB_TITLE]}.mkv\"");
    end
    outfile_moved = $? == 0
    headLine = `head -1 \"#{FILE_TODO}\"`
    otherJobLines = `tail -n +3 \"#{FILE_TODO}\" `

    jobFile = File.open(FILE_TODO, "w")
    jobFile.puts headLine
    jobFile.print otherJobLines #don't create another newline after the last job
    jobFile.close()

    if(outfile_moved)
        #remove the temporary files:
        system("rm -r ./#{dvddir}")
        if($? != 0)
            puts "WARNING: Can't remove temporary files!"
        end
    else
        puts "ERROR when moving the output video file (out.mkv)!"
        puts "NOT removing the job directory #{dvddir}!"
    end
    #put job in the list with done jobs:
    system("echo \"#{createCurrentJobLine()}\" >> " + FILE_DONE)
    $CurrentJob = []
    $jobsProcessed += 1
end




def convertCurrentJob()
    puts "----------------"
    puts "Converting..."
    puts "----------------"

    dvddir = getCurrentJobDirectory()

    if($CurrentJob[JOB_AORDER].length() < 1)
        #undefined extended audio informations: read mplayer audio data first!
        puts 'Audio stream information cannt be read! Aborting!'
        exit 1
    end

    #If an executable script called "before-convert-hook.sh" is placed in the
    #current working directory, it is called and gets the project directory
    #as parameter:
    before_convert_hook_filename = File.expand_path('before-convert-hook.sh', Dir.pwd)
    if (File.exists?(before_convert_hook_filename) and File.executable?(before_convert_hook_filename))
        system("#{before_convert_hook_filename} './#{dvddir}'")
        if ($? != 0)
            puts "ERROR: The before-convert-hook.sh script exited with an error!"
            return false
        end
    end

    Moeripper.readFfmpegAudioStreamInfo('./' + dvddir + '/dvd.mpg')

    #base information
    ffmpeg_prefix = "ffmpeg -sn -i \"./#{dvddir}/dvd.mpg\" "

    ffmpeg_video_params = ''

    ffmpeg_audio_params = ''

    #create one or more video streams first:
    video_i = 0
    $CurrentJob[JOB_PROFILE].videoConfig.each do |vc|

        ffmpeg_video_params += "-codec:v #{vc.codec} -s #{vc.resolution} "

        #video quality (either bitrate or quality)
        if(vc.qualityMode == "q")
            #use -q (for quality) instead of -b (for bitrate):
            ffmpeg_video_params += "-qscale #{vc.qualityParam} "
        else
            #use -b (for bitrate):
            ffmpeg_video_params += "-b:v #{vc.qualityParam} "
        end

        #aspect ratio (depends on the current job instead of the video stream configuration)
        ffmpeg_video_params += "-aspect #{$CurrentJob[JOB_VASPECT]} "

        #frame mode
        case (vc.deinterlace)
        when "p"
            ffmpeg_video_params += "-vf yadif "
        when "s"
            ffmpeg_video_params += "-vf yadif=bff "
        end

        #frame rate and no audio channels:
        ffmpeg_video_params += "-r #{vc.fps} -an "

        #set video fourcc tag if specified in profile:
        if(vc.fourCC != "")
            ffmpeg_video_params += "-vtag #{vc.fourCC} "
        end

        #Get the ffmpeg video stream number:
        ffmpeg_video_info = `ffmpeg -i "./#{dvddir}/dvd.mpg" 2>&1 | grep 'Stream.*Video'`.split("\n")
        if (ffmpeg_video_info.size() < 1)
            puts "ERROR: No video streams in the file!"
            exit 1
        end
        first_video_stream_part = ffmpeg_video_info[0].gsub(" ","").gsub("Stream#0:","").split(":")[0]
        ffmpeg_video_stream_id = Integer(first_video_stream_part.split("[")[0])

        #the mapping information and the output file:
        ffmpeg_video_params += "-map 0:#{ffmpeg_video_stream_id} -y \"./#{dvddir}/video"+video_i.to_s()+".mkv\" "
        video_i += 1
    end

    #now the audio streams:
    #for each audio configuration and each input audio stream:
    $CurrentJob[JOB_PROFILE].audioConfig.each do |ac|
        $CurrentJob[JOB_AORDER].each { |stream_id, stream_data|
            system("mkdir #{dvddir}/audio"+stream_id.to_s()+"/")
            ffmpeg_audio_params += " -vn -async 1 -acodec pcm_s16le -ac #{ac.channels} -ar #{ac.rate} -f wav -map 0:#{stream_data[:ffmpeg_stream_id]} -y \"./#{dvddir}/audio"+stream_id.to_s()+"/audio#{stream_id}.wav\" "
        }
    end

    if ($CurrentJob[JOB_PROFILE].processing_config.two_pass)
        first_run = ffmpeg_prefix + "-pass 1 -passlogfile \"./#{dvddir}/dvd.mpg.log\" " +
                        ffmpeg_video_params
        #DEBUG:
        puts first_run
        system(first_run)
        if($? != 0)
            puts "Error: ffmpeg error on first pass! Abort!"
            exit 1
        end
        second_run = ffmpeg_prefix + "-pass 2 -passlogfile \"./#{dvddir}/dvd.mpg.log\" " +
                     ffmpeg_video_params + ffmpeg_audio_params
        #DEBUG:
        puts second_run
        system(second_run)
        if($? != 0)
            puts "Error: ffmpeg error on second pass! Abort!"
            exit 1
        end
    else
        ffmpeg_command = ffmpeg_prefix + ffmpeg_video_params + ffmpeg_audio_params
        #DEBUG:
        puts ffmpeg_command

        system(ffmpeg_command)
        if($? != 0)
            puts "Error: ffmpeg error! Abort!"
            exit 1
        end
    end

    audioFiles = Dir.glob("./#{dvddir}/audio*/*.wav")

    #DEBUG
    #puts audioFiles.to_s()

    audioFiles.each do |a|
        if($useNormalize)
            system("normalize \"#{a}\"")
            if($? != 0)
                puts "Error: normalize error! Abort!"
                exit 1
            end
        else
            system("normalize-audio \"#{a}\"")
            if($? != 0)
                puts "Error: normalize-audio error! Abort!"
                exit 1
            end
        end
        a = a.gsub(".wav","")

        #handle things different for each audio codec:
        #(nothing to do if the output format is wav)
    end

    $CurrentJob[JOB_PROFILE].audioConfig.each do |ac|
        acFiles = Dir.glob("./#{dvddir}/audio*/*.wav")
        #puts "DEBUG: acFiles="+acFiles.to_s()
        acFiles.each do |a|
            a = a.gsub(".wav","")
            if (ac.codec == "vorbis")
                if(ac.qualityMode == "q")
                    #use quality settings
                    system("oggenc -q #{ac.qualityParam} -o \"#{a}.ogg\" \"#{a}.wav\"")
                else
                    #use bitrate settings
                    system("oggenc -b #{ac.qualityParam} -o \"#{a}.ogg\" \"#{a}.wav\"")
                end
                if($? == 0)
                    system("rm -v \"#{a}.wav\"")
                else
                    puts "Error: oggenc error! Abort!"
                    exit 1
                end
            elsif (ac.codec == "mp3")
                if(ac.qualityMode == "q")
                    #use quality settings
                    mp3Quality = 9 - ac.qualityParam.to_i()
                    system("lame -q #{mp3Quality.to_s()} \"#{a}.wav\" \"#{a}.mp3\"")
                else
                    #use bitrate settings
                    system("lame -b #{ac.qualityParam} \"#{a}.wav\" \"#{a}.mp3\"")
                end
                if($? == 0)
                    system("rm -v \"#{a}.wav\"")
                else
                    puts "Error: lame error! Abort!"
                    exit 1
                end
            else
                if (ac.qualityMode == "q")
                    #use quality settings
                    system("ffmpeg -i \"#{a}.wav\" -codec:a #{ac.codec} -q:a #{ac.qualityParam} -y \"#{a}.mka\"")
                else
                    #use bitrate settings
                    system("ffmpeg -i \"#{a}.wav\" -codec:a #{ac.codec} -b:a #{ac.qualityParam} -y \"#{a}.mka\"")
                end
                if($? == 0)
                    system("rm -v \"#{a}.wav\"")
                else
                    puts "Error: ffmpeg error during audio encoding! Abort!"
                    exit 1
                end
            end
        end
    end


    #run mkvmerge:

    #replace "-characters in the title:
    escapedTitle = $CurrentJob[JOB_TITLE].gsub("\"","\\\"")


    mkvmergeCommand = "mkvmerge --title \"#{escapedTitle}\" -o \"./#{dvddir}/out.mkv\" "

    #video track options:
    for i in (0..$CurrentJob[JOB_PROFILE].videoConfig.length-1)
        if(i == 0)
            mkvmergeCommand += "--default-track 0:yes "
        else
            mkvmergeCommand += "--default-track 0:no "
        end
        mkvmergeCommand += "--forced-track 0:no " +
                           "--aspect-ratio 0:#{$CurrentJob[JOB_VASPECT].gsub(":","/")} " +
                           "-d 0 -A -S \"./#{dvddir}/video"+i.to_s()+".mkv\" "
    end

    #audio track options:
    first_audio_track = true
    $CurrentJob[JOB_AORDER].each { |stream_id, stream_data|
        $CurrentJob[JOB_PROFILE].audioConfig.each { |ac|
            mkvmergeCommand += "--language 0:#{convertLanguageCode(stream_data[:lang])} "
            if (first_audio_track)
                mkvmergeCommand += "--default-track 0:yes "
                first_audio_track = false
            else
                mkvmergeCommand += "--default-track 0:no "
            end
            mkvmergeCommand += "--forced-track 0:no -a 0 -D -S "

            #treat audio codec file extensions:
            case (ac.codec)
            when "vorbis"
                #special treatment for codecs whose file extensions aren't the same as the code name
                mkvmergeCommand += "\"./#{dvddir}/audio"+stream_id+"/audio#{stream_id}.ogg\" "
            when "mp3"
                #special treatment for mp3, too
                mkvmergeCommand += "\"./#{dvddir}/audio"+stream_id+"/audio#{stream_id}.ogg\" "
            else
                mkvmergeCommand += "\"./#{dvddir}/audio"+stream_id+"/audio#{stream_id}.mka\" "
            end
        }
    }

    #add track orders:
    mkvmergeCommand += " --track-order "
    #for video tracks:
    for i in (0..$CurrentJob[JOB_PROFILE].videoConfig.length-1)
        if(i > 0)
            mkvmergeCommand += ","
        end
        mkvmergeCommand += "#{i}:0"
    end

    i = $CurrentJob[JOB_PROFILE].videoConfig.length-1
    #for audio tracks:
    for j in (i..(i+$CurrentJob[JOB_AORDER].length-1))
        mkvmergeCommand += ",#{j}:0"
    end

    #DEBUG:
    system("echo '#{mkvmergeCommand}' > ./#{dvddir}/mkvmergeCommand.sh")

    system (mkvmergeCommand)


    if($?.exitstatus > 1) #mkvmerge returns 0 if everything went fine and 1 if there were warnings. Return code 2 means an error occured
        puts "Error: mkvmerge error! Abort!"
        exit 1
    end

    puts "----------------"
    puts "Done converting!"
    puts "----------------"

    return true
end






def executeJob()
    if(FileTest.exist?(FILE_TODO) != true)
        if (FileTest.exist?(FILE_TODO_OLD))
            puts "The job file name has changed to \"" + FILE_TODO + "\". Please rename \"" + FILE_TODO_OLD + "\"!"
            exit 0
        else
            puts "ERROR: job file ("+FILE_TODO+") not found in this directory!"
            exit 1
        end
    end
    jobFile = File.open(FILE_TODO,"r")
    #line 1:
    if(jobFile.gets == nil)
        puts "ERROR: job file ("+FILE_TODO+") is empty!"
        if($jobsProcessed == 0)
            puts "If you like to have assistance in creating a moeripper job please execure \"moeripper.rb wizard\""
        else
            puts "The job file wasn't empty when the script started. Please make sure that no other program is accessing the job file."
        end
        exit 1
    end

    #read the first job in the file:
    jobLine = ""
    if((jobLine = jobFile.gets) == nil)
        if($jobsProcessed == 0)
            puts "ERROR: todo file ("+FILE_TODO+") has no jobs!"
            puts "If you like to have assistance in creating a moeripper job please execure \"moeripper.rb wizard\""
        else
            puts "No more jobs in job file. Nothing left to do. Have a nice day!"
        end
        exit 1
    end
    job = jobLine.gsub("\n","").split(";")
    if(job[JOB_AORDER] == nil)
        puts "ERROR: todo file ("+FILE_TODO+") is not in the recognised format!"
        exit 1
    end

    $CurrentJob = job
    if(Moeripper.preprocessCurrentJob() != true)
        puts "ERROR: current job is invalid!"
        puts "If you like to have assistance in creating a moeripper job please execure \"moeripper.rb wizard\""
        exit 1
    end

    puts "################"
    puts "JOB INFORMATION:"
    print "Mode:\t\t"
    for i in (0..$CurrentJob[JOB_MODE].length-1)
        if(i > 0)
            print " -> "
        end
        case($CurrentJob[JOB_MODE][i])
        when "R"
            print "Rip"
        when "r"
            print "Rip and wait"
        when "C"
            print "Convert"
        when "c"
            print "Convert and wait"
        else
            print "UNKNOWN"
        end
    end
    puts
    puts "Profile:\t"+$CurrentJob[JOB_PROFILE].name
    puts "DVD name:\t"+$CurrentJob[JOB_DVDNAME]
    puts "DVD track:\t"+$CurrentJob[JOB_DVDTRACK]
    puts "Title:\t\t"+$CurrentJob[JOB_TITLE]
    puts "Index:\t\t"+$CurrentJob[JOB_INDEX]

    puts "----------------"
    puts "Video configuration:"
    puts "Aspect ratio:\t\t"+$CurrentJob[JOB_VASPECT]
    puts ""
    i = 0
    $CurrentJob[JOB_PROFILE].videoConfig.each do |vc|
        puts "Output video configuration #"+i.to_s()+":"
        puts "\tResolution:\t\t"+vc.resolution
        puts "\tCodec and Quality:\t"+vc.codec+" @ " + vc.qualityMode + " " + vc.qualityParam
        case(vc.deinterlace)
        when "p"
            puts "\tFrames:\t\t\tprogressive"
        when "i"
            puts "\tFrames:\t\t\tinterlaced"
        when "s"
            puts "\tFrames:\t\t\tprogressive with fields swapped"
        else
            puts "\tFrames:\t\t\tunknown"
        end
        puts "\tFrames per second:\t"+vc.fps+"\n"
        i += 1
    end

    puts "----------------"
    puts 'Audio track order:'
    puts
    job[JOB_AORDER].each { |stream_id, stream_data|
        order_string = stream_id + '(' + stream_data[:lang] + ')'
        if (stream_data[:ffmpeg_stream_id])
            order_string += ' => ' + stream_data[:ffmpeg_stream_id]
        end
        puts(order_string)
    }
    puts

    i = 0
    $CurrentJob[JOB_PROFILE].audioConfig.each do |ac|
        puts "Output audio configuration #"+i.to_s()+":"
        puts "\tSample rate:\t\t"+ac.rate
        puts "\tAudio channels:\t\t"+ac.channels
        puts "\tCodec and Quality:\t"+ac.codec+" @ " + ac.qualityMode + " " + ac.qualityParam
        i += 1
    end

    puts

    puts "################"

    status = false

    while ($CurrentJob[JOB_MODE].length > 0)
        status = false
        case($CurrentJob[JOB_MODE][0].upcase())
        when "R"
            status = Moeripper.ripCurrentJob()
        when "C"
            status = convertCurrentJob()
        end
        if(status == true)
            if($CurrentJob[JOB_MODE][0].downcase() == $CurrentJob[JOB_MODE][0])
                #the job is written with a lowercase
                #confirmation is required before the job is finished:
                puts "Press enter to continue..."
                gets
            end
            $CurrentJob[JOB_MODE].delete_at(0)

            #update the job list:
            #in case something goes wrong in the next step
            #we don't have to redo the ripping process
            updateCurrentJob()
        else
            print "ERROR when "
            case($CurrentJob[JOB_MODE][0].upcase())
            when "R"
                print "ripping!\n"
            when "C"
                print "converting!\n"
            end
            exit 1
        end
    end

    #the current job is done here.
    #finish it (cleaning up)
    finishCurrentJob()
    return true
end


def main()
    doWizard = false

    if(ARGV.length == 1)
        case(ARGV[0])
        when "convertprofiles"
            convertProfileFile()
            exit 0
        when "checkinstall"
            Moeripper.checkInstallation()
            exit 0
        when "wizard"
            doWizard = true
        when "version"
            puts "Moeripper.rb version "+MOERIPPER_VERSION
            exit 0
        when "help"
            Moeripper.showHelp()
            exit 0
        end
    elsif(ARGV.length > 1)
        puts "ERROR: too much arguments!"
        exit 1
    end

    readProfiles()

    if(doWizard == true)
        jobWizard()
        exit(0)
    end

    #check if normalize or normalize-audio is installed and
    #set the global variable for the selection of the right executable.

    system("normalize --version > /dev/null 2>&1")
    if($? != 0)
        #try normalize audio!
        $useNormalize = false
        puts "normalize seems not to be installed, using normalize-audio instead!"
    end

    #do all jobs until there are no more
    while(executeJob() == true) #DEVELOPMENT!
    end
end


main()

#puts ARGV[0].to_s() + " -> " + convertLanguageCode(ARGV[0].to_s()).to_s() # a test for the convertLanguageCode function
