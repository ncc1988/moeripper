/*
    This file is part of moeripper.
    Copyright (C) 2014-2024  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <string>


namespace Moeripper
{
    class VideoConfig
    {
        public:


        std::string resolution;


        uint8_t fps;


        bool use_deinterlace;


        std::string codec;


        uint16_t bitrate;


        float quality;


        bool use_quality;


        std::string fourcc;


        VideoConfig(const std::string config_string);
    };
}
