# Moeripper

## Introduction

Moeripper is a DVD ripping solution for bulk ripping of DVDs. In cases where the ripping shall be an automated process Moeripper becomes handy with its batch processing of ripping jobs.

By using Moeripper it is easy to rip DVDs with multiple audio tracks into one output file (using the Matroska container format). Although it is a program designed for the unix shell it has a wizard to help you create a new ripping job.

Moeripper lets you define profiles where each profile represents a set of audio and video settings. With these profiles you can also create multiple audio or video tracks out of one track. This might be useful when you want one "high quality" video stream and a second "low quality" stream for old computers. Of course, the same thing is possible for audio streams.


## Installation

### Requirements

The operating systems must meet the following requirements:

- Unix or Unix-like operating system (like Linux)
- Ruby > 1.9
- The GNU C++ compiler (g++)
- libdvdread with development headers (package "libdvdread-dev" on Debian GNU/Linux and derivatives)
- libfmt with development headers (package "libfmt-dev" on Debian GNU/Linux and derivatives)


Moeripper requires the following programs to run properly and to be placed inside a path for executable files:

- mpv
- ffmpeg
- normalize-audio
- vorbisgain
- mp3gain
- mkvmerge (from mkvtoolnix)
- moeripper_cpp (can be compiled using "make" in the same directory of the source code where this file is placed)

To be able to rip a DVD one should have at least 20 GiB of free space available. This space is needed for temporary files during the ripping process.

### Installation itself

The C++ part of moeripper needs to be compiled first using the Makefile in the root of the source code directory. Just run "make" to compile it and then place it in one of the paths for executable files. For example, this can be the "bin" directory in your home directory (~/bin/).

The moeripper.rb script can be placed in a directory where you want to have the temporary files from DVD ripping and Moeripper will work just fine after you copied the configuration files (see below).

However, if you feel more comfortable with it you may place the moeripper.rb script into any directory for executable files.

### Copying configuration files

Before you can start using Moeripper you need to copy the configuration files into the folder $HOME/.config/moeripper/. At the moment the configuration consists of the file Profiles.csv which contains all defined profiles for audio and video conversion.

In case you just want to get Moeripper working you may execute the script install-profiles.sh in your copy of the Moeripper source code repository. It will create the directory $HOME/.config/moeripper/ for your and copy the Profiles.csv file in there.


That's it, Moeripper is ready to rip :)


## Extra: before-convert-hooks

Moeripper has the ability to execute a shell script before the conversion of
a file starts. This may become handy when the ripped DVD track cannot directly
be processed by ffmpeg.
For example, when a DVD track has unwanted additional content at the end
(DVD menu frames or DVD images like copyright warnings), these can be removed
by a script that cuts the end of the DVD track.

If a before-convert hook shall script shall be called, it must be placed in
the current working directory and it must be named "before-convert-hook.sh".

The script gets the project directory relative to the current working directory.
If your project is named "bunny-1", the script will be called by moeripper
like this:

    before-convert-hook.sh ./bunny-1

If the before-convert-hook.sh script returns with a non-zero value, moeripper
will not continue with the conversion and exit to prevent wrong conversions.

Shell scripts that can be used as before-convert hooks are available in the
"before-convert-hooks" directory.
